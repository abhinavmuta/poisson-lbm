import numpy as np
from numpy import sin, cos, pi, exp
import scipy.sparse as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def fd_diff(n, coeffs, diag_idx, periodic=False):
    """
    Construct a finite difference matrix given the coefficients on the
    diagonals and their respective diagonal index

    Parameters
    ----------
    n : Int
        No of grid points.
    coeffs: Array
        Coefficients of the finite difference discretization.
    diag_idx: Array
        Indices of the diagonal of the respective coefficients, e.g. 0 if on
        principle diagonal, `1` if above the principle diagonal, `-1` if below.
    """
    D = np.zeros([n, n])
    for i in range(len(coeffs)):
        j = n - abs(diag_idx[i])
        D += np.diag([coeffs[i]] * j, diag_idx[i])
        if periodic:
            idx = diag_idx.index(-diag_idx[i])
            if diag_idx[idx] < 0:
                j = -n - diag_idx[idx]
            D += np.diag([coeffs[idx]] * abs(diag_idx[idx]), j)
    return D



def interp1d(n):
    T = fd_diff(2*n-1, [0.5, 1, 0.5], [1, 0, -1])
    return T[:, 1::2]


def interp2d(n):
    T = interp1d(n);
    return np.kron(T, T)


def poisson_stencil1d(n):
    h = 1/n
    T = fd_diff(n-1, [-1, 2, -1], [1, 0, -1])
    return T/h


def poisson_stencil2d(n):
    h = 1/n
    T = fd_diff(n-1, [-1, 2, -1], [1, 0, -1])
    I = np.eye(n-1)
    D2 = np.kron(T, I) + np.kron(I, T)
    return D2/(h**2)


def relaxJacobi(L, u, f, omega):
    u_next = u + omega/np.diag(L) * (f - L@u)
    return u_next


def relaxGS(L, u, f, omega):
    LL = np.tril(L)
    u = np.linalg.solve(LL, f-np.triu(L)@u)
    return u


def restrictionFW1d(n):
    if np.mod(n, 2) != 0:
        raise Exception('n must be even')

    T = fd_diff(n-1, [1, 2, 1], [1, 0, -1])
    return T[1::2]/4


def restrictionFW2d(n):
    R = restrictionFW1d(n)
    return np.kron(R, R)


def test_poission_eig2d(n):
    A = poisson_stencil2d(n)
    A = sp.csr_matrix(A)
    x = np.arange(1, n)/n
    err = 0
    for k in range(0, n-1):
        for l in range(0, n-1):
            phi = (2/n) * np.kron(sin(k*pi*x), sin(l*pi*x))
            lam = 2*(1-cos(k*pi/n)) + 2*(1-cos(l*pi/n))
            err += np.linalg.norm(A@phi - n**2*lam*phi)
    return err


def multigrid_cycle(n, gamma, u, Lop, f, nu1, nu2, opts):
    if np.mod(n, 2) != 0:
        raise Exception("n must be a power of 2")

    L = Lop(n)
    if opts.lower() == 'jacobi':
        omega = 4/5
        smooth = relaxJacobi
    elif opts.lower() == 'gs':
        omega = 1.0
        smooth = relaxGS

    for i in range(nu1):
        print("b4")
        print(n)
        u = smooth(L, u, f, omega)

    if n > 2:
        R = restrictionFW2d(n)
        res = f - L@u
        res = R@res

        v = np.zeros((n//2-1)**2)
        for j in range(1, gamma+1):
            v = multigrid_cycle(n//2, gamma, v, Lop, res, nu1, nu2, opts)
        v = interp2d(n//2)@v

        u = u + v

    for j in range(nu2):
        print("a4")
        print(n)
        u = smooth(L, u, f, omega)
    return u


def test_convergence(scheme):
    n = 1<<6
    x, y = np.mgrid[1:n, 1:n] / n
    f = exp(-cos(4*x)**2 + exp(-sin(6*y)**2)) - 3/2

    fig, ax = plt.subplots(subplot_kw=dict(projection='3d'))
    ax.plot_surface(x, y, f, cmap=plt.cm.Spectral)
    ax.contour(x, y, f, cmap=plt.cm.Spectral, offset=-1)
    # plt.show()

    f = f.ravel()
    u_exact = poisson_stencil2d(n)
    u_exact = np.linalg.solve(u_exact, f)
    u_exact = u_exact.reshape(n-1, n-1)

    fig, ax = plt.subplots(subplot_kw=dict(projection='3d'))
    ax.plot_surface(x, y, u_exact, cmap=plt.cm.Spectral)
    ax.contour(x, y, u_exact, cmap=plt.cm.Spectral, offset=-1)
    u_exact = u_exact.ravel()
    # plt.show()

    maxiter = 3
    nu1 = 1
    nu2 = 1
    err = []
    for gamma in range(1, 3):
        print(gamma)
        errG = np.zeros(maxiter+1)
        u = np.zeros((n-1)**2)
        errG[0] = np.linalg.norm(u-u_exact)
        for j in range(maxiter):
            print("starting iteration ", j)
            u = multigrid_cycle(n, gamma, u, poisson_stencil2d, f, nu1, nu2,
                                scheme)
            errG[j+1] = np.linalg.norm(u-u_exact)
        err += [errG]

    fig, ax = plt.subplots()
    ax.semilogy(range(maxiter+1), err[0], '.-', label=r'$\gamma=1$')
    ax.semilogy(range(maxiter+1), err[1], '*-', label=r'$\gamma=2$')
    plt.legend()
    plt.show()



if __name__ == '__main__':
    test_convergence('jacobi')
    # test_convergence('gs')

