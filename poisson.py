import numpy as np
from numpy import exp, pi, sinh, cos
import matplotlib.pyplot as plt


def exact1d(nx):
    dx = 1.0/nx
    x = np.arange(0, 1+dx, dx)
    u = np.zeros_like(x)
    u[0] = 1.0
    u[-1] = 1.0
    k = 27.79

    ek = exp(k)
    eki = exp(-k)
    fac = 1.0 / (ek - eki)
    u[1:-1] = fac * ((ek-1.0) * exp(-k*x[1:-1]) + (1.0-eki) * exp(k*x[1:-1]))
    return u


def exact2d(nx, ny):
    x, y = np.mgrid[0:1:nx*1j, 0:1:ny*1j]
    u = np.zeros_like(x)

    mu = np.sqrt(4.0 + pi*pi)
    u[0,:] = sinh(mu*(1-y[0,:])) / sinh(mu)
    u[1,:] = -1 * sinh(mu*(1-y[1,:])) / sinh(mu)
    u[:,0] = cos(pi*x[:,0])
    u[:,1] = 0.0

    u = cos(pi*x) * sinh(mu*(1-y)) / sinh(mu)
    return u


def lbm(nx, iters=100):
    dt = 1.0/nx
    c = 1.0
    tau = 1.0
    omega = 1.0/tau
    D = c*c*(0.5 - tau) * dt/3.0
    k = 27.79

    wts = [2.0/3.0, 1.0/6.0, 1.0/6.0]

    u = np.zeros(nx+1)
    u_exact = exact1d(nx)
    f = np.zeros((3, nx+1))

    # f = feq
    f[0] = (wts[0] - 1.0) * u
    for i in range(1, 3):
        f[i] = wts[i] * u

    # Bc
    u[0] = 1.0
    u[-1] = 1.0

    for i in range(iters):
        # Streaming step
        f[1, :-1] = f[1, 1:]
        f[2, 1:] = f[2, :-1]

        # Bc's
        # u[0] = 1.0
        # u[-1] = 1.0

        f[1, 0] = wts[1] * u[0] + (1.0 - omega) * (f[1, 1] - wts[1] * u[1])
        f[2, -1] = wts[2] * u[-1] + (1.0 - omega) * (f[2, -2] - wts[2] * u[-2])

        u = np.sum(f[1:], axis=0)/(1.0 - wts[0])

        # GRE = np.sum(abs(u_exact -) u)) / np.sum(np.abs(u_exact))
        # print(GRE)
        # print(max(abs(u_exact-u)), np.average(abs(u_exact-u)))
        if (max(abs(u_exact - u)) < 1e-2):
            print("Steady state reached")
            break

        # Collision step
        f[0] = (1.0-omega) * f[0] + omega*(wts[0] - 1.0)*u
        for i in range(1, 3):
            f[i] = (1.0-omega) * f[i] + (omega*wts[i] + dt*0.5*D*k*k) * u

        u[0] = 1.0
        u[-1] = 1.0
    return u_exact, u


def lbm2d(nx, ny, iters=100):
    wts = [0, 0.25, 0.25, 0.25, 0.25]
    alpha = 0.5

    x, y = np.mgrid[0:1:nx*1j, 0:1:ny*1j]
    dt = 1.0/nx
    dx = 1.0/nx
    c = dx/dt
    tau = 1.0
    omega = 1.0/tau
    D = alpha*c*c*(0.5 - tau) * dt

    u = np.zeros_like(x)
    u_exact = exact2d(nx, ny)
    f = np.zeros((5, nx, ny))

    # f = feq
    f[0] = (wts[0] - 1.0) * u
    for i in range(1, 5):
        f[i] = wts[i] * u

    # Bc
    mu = np.sqrt(4.0 + pi*pi)
    u[0,:] = sinh(mu*(1-y[0,:])) / sinh(mu)
    u[1,:] = -1 * sinh(mu*(1-y[1,:])) / sinh(mu)
    u[:,0] = cos(pi*x[:,0])
    u[:,1] = 0.0

    for i in range(iters):
        # Streaming step
        f[1, :-1] = f[1, 1:]
        f[3, 1:] = f[3, :-1]

        f[2, :, :-1] = f[2, :, 1:]
        f[4, :, 1:] = f[4, :, :-1]

        u[0,:] = sinh(mu*(1-y[0,:])) / sinh(mu)
        u[1,:] = -1 * sinh(mu*(1-y[1,:])) / sinh(mu)
        u[:,0] = cos(pi*x[:,0])
        u[:,1] = 0.0

        f[3, 0] = wts[3] * u[0] + (1.0 - omega) * (f[3, 1] - wts[3]*u[1])
        f[1, -1] = wts[1] * u[-1] + (1.0 - omega) * (f[1, -2] - wts[1]*u[-2])
        f[4, :, 0] = wts[4] * u[:, 0] + (1.0 - omega) * (f[4, :, 1]
                                                         - wts[4]*u[:, 1])
        f[2, :, -1] = wts[2] * u[:, -1] + (1.0 - omega) * (f[2, :, -2]
                                                           - wts[2]*u[:, -2])

        u = np.sum(f[1:], axis=0)/(1.0 - wts[0])

        # GRE = np.sum(abs(u_exact -) u)) / np.sum(np.abs(u_exact))
        # print(GRE)
        # print(abs(u_exact-u).max(), abs(u_exact-u).min())
        if (abs(u_exact - u).max() < 1e-4):
            print("Steady state reached")
            break

        # Collision step
        f[0] = (1.0-omega) * f[0] + omega*(wts[0] - 1.0)*u
        for i in range(1, 5):
            f[i] = (1.0-omega) * f[i] + (omega*wts[i] + dt*0.25*D*4) * u

    return u_exact, u


def main1d():
    nx = 100
    dx = 1.0/nx
    iters=100
    grid = np.arange(0, 1+dx, dx)
    u_exact, u_lbm = lbm(nx, iters)

    # for i in np.arange(0, 1.1, 0.1):
        # print(i, u_exact[grid==i], u_lbm[grid==i])

    plt.plot(grid, u_exact, label='analytical')
    plt.plot(grid, u_lbm, '*', label='lbm')
    plt.legend(loc='best')
    plt.xlim(0, 1)
    plt.ylim(0, 1)
    plt.show()

def main2d():
    nx = 100
    ny = nx
    iters = 10000
    x, y = np.mgrid[0:1:nx*1j, 0:1:ny*1j]
    u_exact, u_lbm = lbm2d(nx, ny, iters)
    plt.subplot(121)
    plt.contourf(x, y, u_exact, 50)
    plt.colorbar(orientation='horizontal')
    plt.subplot(122)
    plt.contourf(x, y, u_lbm, 50)
    plt.colorbar(orientation='horizontal')
    plt.show()


if __name__ == '__main__':
    main1d()
    main2d()
